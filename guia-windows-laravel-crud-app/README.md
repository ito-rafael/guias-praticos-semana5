O guia abaixo tem por objetivo ensinar o desenvolvimento de uma aplicação no back-end em [Laravel 8.54](https://laravel.com/) com o  banco de dados MySQL. O principal objetivo é criar uma API que realiza CRUD de País e Universidades. Este guia é para ambiente Windows (64 Bits).

## Ambiente Windows (Versão 64 Bits)

Toda a configuração do ambiente deve ser feita utilizando o framework Laragon, que foi discutido na segunda semana do curso. Este framework, como já discutido, engloba PHP, Apache, MySQL e diversas outras ferramentas, o que facilita o desenvolvimento da aplicação Web, pois não precisamos fazer todas essas configurações como são feitas no ambiente Linux. Vamos então colocar a mão na massa!!

### Executando o Laragon

Considero que você já tenha o Laragon instalado, conforme discutimos no guia prático da Semana 2. Execute o Laragon conforme a tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon1.png" alt="Inicializar o Laragon."/>
</p>

Na sequência, será apresentada uma tela com o painel completo do Laragon, como mostra a figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon2.png" alt="Painel do Laragon."/>
</p>

Clique no botão "Iniciar tudo" para que o MySQL e o PHP entrem em operação, como podemos verificar na figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon3.png" alt="Inicialização de serviços no Laragon."/>
</p>

Temos que lembrar que estamos em ambiente Windows, e podemos fazer os ajustes no Laragon tanto no terminal de comando do Windows (CMD) quanto no terminal que é proporcionado pelo Laragon. Por questão de facilidade, eu utilizarei o terminal do Laragon. Para isso, clique no botão terminal no painel do Laragon e terá um terminal aberto para que possamos interagir com o framework, instalar o Laravel e também condificar a aplicação do back-end.

<p align="center">
  <img target="_blank" src="./imagens/laragon4.png" alt="Terminal do Laragon."/>
</p>

Vocês podem verificar também via Windows Explorer os arquivos para o funcionamento do Apache e a pasta onde colocaremos nossa aplicação, como pode ser observado na figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon5.png" alt="Diretório do Apache."/>
</p>

Uma vez tendo acesso ao terminal do Laragon, o próximo passo é instalar o composer.

### Instalando o composer
Com o terminal do Laragon aberto, e na pasta WWW, digite o comando a seguir para baixar  o composer utilizado o curl:
```
curl -sS https://getcomposer.org/installer -o composer-setup.php
```
Na sequência, execute o seguinte comando e instale o composer globalmente na sua máquina/host:
```
composer global require laravel/installer
```
Desta forma o composer está instalado na sua máquina. Ao terminar basta você executar o comando abaixo:
```
composer -V
```
e visualizar a versão do composer instalada.

### Criando projeto Laravel
Ainda no terminal do Laragon, na pasta WWW, crie o projeto da nossa aplicação com o comando a seguir:
```
composer create-project laravel/laravel --prefer-dist laravel-crud-app
```

O processo de instalação tem como saída a o que é mostrado na imagem a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon6.png" alt="Processo de criação da aplicação com o Laravel."/>
</p>

Para criar um projeto laravel, atente-se para o "laravel-crud-app", uma vez que esse é o nome do projeto que deve ser substituido antes de executar o comando. Após executar o comando anterior, basta acessar a pasta do projeto:
```
cd laravel-crud-app
```
E verificar a versão do laravel instalada:
```
php artisan -V
```
### Configurando o MySQL
Nesse passo será apresentado como se faz a conexão entre o banco de dados MySQL na aplicação do framework do Laravel. Lembre-se que as variáveis de nome do banco de dados, usuário e senha do banco devem ser ajustadas de acordo com as configurações presentes na máquina e o nome do banco de dados que você criou. Como já temos o MySQL rodando no Laragon, o que temos que fazer é criar um banco de dados para o nosso CRUD. Por padrão, o Laragon utiliza o root como usuário, e não coloca senha. Para fins de ensino, não vamos nos preocupar com isso no momento, muito embora em um projeto maior e complexo seja obrigatório definir uma senha forte para o usuário do banco de dados, seja ela root ou não. Então, neste guia, o usuário será  **root** e com senha em branco. Abra o terminal do Laragon e digite o comando:

```
mysql -u root
```
E teremos a tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon7.png" alt="Acessando o MySQL Server."/>
</p>

A configuração e a criação do banco de dados devem ser realizadas antes de executar as migrações do Laravel. Você deve criar um banco e dar um nome que reflete o que o sistema fará. Para este guia, consideramos a instalação e configuração do Laravel (Semana 2), e demos o nome do banco de `laravel_db`. Por isso seguimos com este banco já criado anteriormente. Caso você tenha esquecido de criar o banco, vamos mostrar como fazê-lo novamente. Considerando que está no prompt do MySQL, digite o comando a seguir:

```
create database laravel_db;
```

e terá como saída a tela mostrada abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon8.png" alt="Criação do banco de dados da aplicação"/>
</p>

Saia do servidor mysql, digitando:

```
quit;
```
Para configurar o banco de dados no laravel, o arquivo **.env** deve ser editado. Atente-se aqui para o fato de que o arquivo `.env` não sofre commit, por que as configurações dos serviços são individuais de cada máquina e normalmente essas variáveis em uma aplicação final são atribuidas diretamente por variáveis de ambiente do SO (Sistema Operacional). Se o arquivo `.env` não existir no projeto baixado de um repositório do git, basta copiar o `.env.example` e atribuir as respectivas variáveis necessárias.

Considerando que você esteja no diretório corrente do projeto (laravel-crud-app), abra o arquivo .env com o seguinte comando:

```
nano .env
```

Altere a linha **DB_DATABASE=laravel** para **DB_DATABASE=laravel_db** de modo que, após realizar a mudanças no `.env`, o código deve ficar como abaixo:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel_db
DB_USERNAME=root
DB_PASSWORD=
```
Deixe a senha em **BRANCO**. Salve as modificações com o comando: CTRL+X e depois digite Y e na sequência tecle ENTER. Pronto, o arquivo foi salvo!!!

Além disso é interessante executar o seguinte comando para limpar o cache que pode existir na aplicação:
```
php artisan config:clear
```

Atente-se para uma configuração adicional que deve ser feita caso o sistema macOS seja utilizado para o desenvolvimento.
```
DB_SOCKET=/Applications/MAMP/tmp/mysql/mysql.sock
```

Como já discutido na semana anterior, e apenas para relembrar, vamos utilizar o modelo de entidade e relacionamento conforme a figura  abaixo:

<p align="center">
  <img target="_blank" src="./imagens/modelo_relacionamento_univesp.drawio.png" alt="Modelo de entidade e relacionamento."/>
</p>

Este modelo contém três tabelas: `Usuário`, `País` e `Universidade`. Além disso, cada Universidade está vinculada a um país, e dessa forma existe uma chave estrangeira de país em Universidade. Como base neste exemplo do modelo, podemos exibir como são mapeadas as entidades relacionadas em Laravel.

### Criando uma Migração e um Modelo
Até este ponto, criamos e configuramos o banco de dados adicionando as credenciais no arquivo `.env`. Em seguida,  aprenderemos como definir a migração adicionando as propriedades de dados na tabela MySQL. Uma migração Laravel é a definição de uma ação que altera o banco de dados com os comandos da DDL (Data Definition Language) criação de tabela, alteração de coluna, exclusão de base de dados, etc. A vantagem é que o processo de desenvolvimento ser torna mais ágil.

Precisamos criar um arquivo de modelo e migração para criar as migrações. Para esta etapa, execute os comandos a seguir. É preciso que você esteja no terminal do Laragon e na pasta laravel-app-crud, como mostrado abaixo:

```
php artisan make:model Pais -m
php artisan make:model Universidade -m

```
<p align="center">
  <img target="_blank" src="./imagens/laragon9.png" alt="Criação do modelo e migração"/>
</p>

Dentro do projeto Laravel, foram criados arquivos no diretório: `database/migrations`. Por padrão, o Laravel criou arquivos com o seguinte padrão:

- timestamp_create_pais_table.php
- timestamp_create_universidades_table.php

Isso pode ser observado na imagem a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon10.png" alt="Arquivos resultantes da migração"/>
</p>

Especificamente para arquivo de modelo de dados do País temos o seguinte código:

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pais', function (Blueprint $table) {
            $table->bigIncrements('id');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pais');
    }
}
```

De acordo com o que foi definido no modelo de dados de país, devemos adicionar o campo `nome`. Para realizar essa adição, basta colocar o seguinte código na função de `up()`. Abra o arquivo **2021_10_21_161712_create_pais_table.php** com o comando nano: 

```
nano 2021_10_21_161712_create_pais_table.php 
```

e edite-o com adicionando o campo nome na função:

```
$table->string('nome');
```

O resultado final pode ser visto na tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon11.png" alt="Edição do modelo Pais"/>
</p>

Já o mapeamento da migração para Universidade necessita da adição de outros campos no método `up()`. Para realizar essa adição, basta colocar o seguinte código na função de `up()`. Abra o arquivo **2021_10_21_161747_create_universidades_table.php** com o comando nano: 

```
nano 2021_10_21_161747_create_universidades_table.php 
```
e edite-o, adicionando os campos nome, descrição, dt_fundacao, e uma chave estrangeira na função:

```
$table->string('nome');
$table->string('descricao', 600);
$table->date('dt_fundacao');
$table->foreignIdFor(Pais::class); // pais_id
```
O resultado final pode ser visto na tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon12.png" alt="Edição do modelo Universidades"/>
</p>

Aqui, definimos um campo do tipo data para a data da fundação da universidade, e definimos um tamanho de 600 para o varchar do campo no MySQL. E por fim, uma chave estrangeira para país é adicionada à universidade. Você deve também adicionar, no arquivo 2021_10_21_161747_create_universidades_table.php, a linha:

```
use App\Models\Pais;
```
logo após o <?php, indicando a importação da classe pais no migrate de universidade.

A classe de universidade, ao final dessa mudança, fica da seguinte forma:
```
<?php

use App\Models\Pais;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universidades', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('descricao', 600);
            $table->date('dt_fundacao');
            $table->foreignIdFor(Pais::class); // pais_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universidades');
    }
}
```

E a classe de Pais, depois da edição, fica da seguinte forma:
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pais', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pais');
    }
}
```

Dentro das migrações há duas funções que são herdadas de `Illuminate\Database\Migrations\Migration;`. Essas funções são `up()` e `down()`. A função `up()` é executada quando a migração for realizada com o comando `php artisan migrate`. No caso do código acima, a migração cria uma tabela chamada `país`, que tem atributos `id`, `nome` e os timestamps padrões para data/hora da criação e da ultíma edição. O método `down()` existe para reverter as ações realizadas pelo método `up()`, ou seja, aqui estão os comandos para dropar tabela, colunas e outros dados.

Ao terminar de configurar as migrações, vamos executar o comando abaixo no terminal do Laragon, sempre na pasta laravel-crud-app: 

```
php artisan migrate
```

O resultado final é a criação das tabelas no banco de dados.

A seguir, será preciso entrar em app/Models, considerando que você esteja no diretório raiz da aplicação: laravel-crud-app. Execute o comando:

```
cd app/Models
```

Agora vamos editar os modelos. Primeiro, vamos fazer este procedimento no arquivo Pais.php. Abra o arquivo com o comando:

```
nano Pais.php
```

O código inicial do modelo será igual a esse:
```
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;
}
```

Deve ser adicionada a variável protegida `$fillable`. O laravel usa o modelo `mass-assignable`, que permite mapear quais os valores serão mapeados para o cadastro diretamente no banco de dados. Isso significa que, se passar um array com o valor determinado no `$fillable`, ele será mapeado para uma coluna na tabela mapeada para o modelo. Por exemplo, adicionando o `$fillable` no modelo Pais, o conteúdo final do arquivo ficará como a seguir:

```
<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
  use HasFactory;
  protected $fillable = ['nome'];
}
```

Salve o arquivo com o comando: CTRL+X e seguinda digite Y quando perguntado se deseja salvar. Tecle ENTER. Pronto!!!

Para o código de Universidade, use o comando:
 
```
nano Universidade.php
```

e adicione o seguinte conteúdo abaixo de use HasFactory:

```
  protected $fillable = [
      'nome',
      'descricao',
      'dt_fundacao',
      'pais_id'
  ];

  // protected $hidden = ['pais_id'];
```

O arquivo final deve ficar como segue:

```
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Universidade extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'descricao',
        'dt_fundacao',
        'pais_id'
    ];

    // protected $hidden = ['pais_id'];
}
```

Salve o arquivo com o comando CRTL+X e depois digite Y para confirmar. Tecle ENTER. Pronto!!!

Veja que é possível ver as colunas mapeadas em `$fillable`. Há também uma nova variável chamada de `$hidden`. Esta variável `$hidden` é responsável por ocultar o campo quando existir uma consulta que recupera os dados dessa tabela. Essa variável é importante para quando se quer ocultar informações do serviço.

Agora vamos apresentar o mapeamento da migração da entidade Universidade. Conforme o modelo do banco de dados definido, precisamos dos campos: `id`, `nome`, `descricao`, `dt_fundacao`, `pais_id` e os campos de data de criação e edição. O campo `pais_id` é uma chave-estrangeria de país. Vá para o diretório database/migrations e abra o arquivo **2021_10_21_161747_create_universidades_table.php**. 

```
cd  /database/migrations

nano 2021_10_21_161747_create_universidades_table_create_universidades_table.php
```

Veja como ele ficou: 

```
<?php

use App\Models\Pais;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universidades', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('descricao', 600);
            $table->date('dt_fundacao');
            $table->foreignIdFor(Pais::class); // pais_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universidades');
    }
}
```

Uma chave-estrangeira é mapeada normalmente como `nome_da_table_id`. Esse é o padrão adotado no Laravel. 

### Controllers
Nos controladores estão os códigos lógicos da aplicação e as ações para cada modelo. Um novo controller pode ser criado executando o código abaixo, utilizando o terminal do Laragon:

```
php artisan make:controller PaisController --resource
php artisan make:controller UniversidadeController --resource
```

O resultado da execução é mostrado a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon13.png" alt="Criação dos controllers da aplicação"/>
</p>

Os comandos anteriores criam respectivamente os controllers: `app/Http/Controllers/PaisController.php` e `app/Http/Controllers/UniversidadeController.php`. O comando `--resource` cria por padrão sete métodos definidos para cada um dos controllers que criamos:

- index(): lista todos os dados do controller.
- create(): retorna a página de formulário para criação do controller. `(não utilizado em API's)`
- store(): usado para criar um novo registro no banco de dados.
- show(): usado para exibir um registro específico no banco de dados.
- edit(): retorna a página de formulário para alterar do controller. `(não utilizado em API's)`
- update(): usado para atualizar um registro específico no banco de dados.
- destroy(): usado para deletar um registro no banco de dados.

Mais especificações sobre [Controllers no Laravel](https://laravel.com/docs/8.x/controllers) podem ser visualizados no link da documentação oficial.

#### Métodos do Controller Pais

Vá para o diretório app/Http/Controllers/ e em seguida procure pelo arquivo **PaisController.php**:

```
cd app/Http/Controllers/
```

<p align="center">
  <img target="_blank" src="./imagens/laragon14.png" alt="Pasta dos controllers da aplicação"/>
</p>

Abra um dos arquivos, por exemplo, o **PaisController.php** e em seguida o **UniversidadeController.php** e verá que os conteúdos dos métodos estão vazios. 

Antes de editar os métodos, devemos incluir duas linhas no começo do arquivo app/Http/Controller/PaisController.php. As primeiras linhas deste arquivo fica assim:

```
namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
```



O próximo passo é editar esses métodos em ambos os arquivos para lidar com o escopo da nossa aplicação.

Vamos tratar de alguns métodos no PaisController.php. O método de `index()` por exemplo é bem simples, pois a ideia é que ele retorne tudo do controller específico. Edite o método `index()`, que deve ficar como segue: 

```
public function index()
{
    return response()->json(
        Pais::all()
    );
}
```

Na sequência, vamos editar o método `store()`. Este é utilizado para criar um novo registro. Nele devemos validar os dados que foram enviados na requisição POST, realizar o cadastro e por fim retornar o dado cadastrado e o status 201. O código do store deve ficar como mostrado abaixo:
```
public function store(Request $request)
{
    $storeData = Validator::make($request->all(), [
      'nome' => 'required|max:255'
    ]);

    if ($storeData->fails()) {
      return response()->json($storeData->messages(), 422);
    }

    return response()->json(
      Pais::create($request->all()),
      201
    );
}
```

Vamos prosseguir, com o método `show()`. Ele é utilizado para recuperar um registro específico. O código deste método, para recuperar um único registro deve ficar como mostraro abaixo:
```
public function show($id)
{
    return response()->json(
        Pais::findOrFail($id)
    );
}
```

Vamos então lidar com o método `update()`, o qual é utilizado para atualizar um registro. Nele são recebidos os dados e o 'id' do registro que deve ser atualizado. Atualize o código como mostrado abaixo:
```
public function update(Request $request, $id)
{
  $updateData = Validator::make($request->all(), [
    'nome' => 'required|max:255'
  ]);

  if ($updateData->fails()) {
    return response()->json($updateData->messages(), 422);
  }

  $pais = Pais::whereId($id)->update([
    "nome" => $request->input('nome')
  ]);
  return response()->json($pais);
}
```

Por fim, chegamos ao método `destroy()`, cujo objetivo é remover um registro específico de acordo com o id informado. Atualize este método de forma que ele fique como mostrado abaixo:
```
public function destroy($id)
{
  $pais = Pais::findOrFail($id);
  $pais->delete();
  return response()->json(null, 204);
}
```

#### Métodos do Controller Universidade

Vá para o diretório app/Http/Controllers/ e em seguida procure pelo arquivo **UniversidadeController.php**:

```
cd app/Http/Controllers/
```

Vamos tratar de alguns métodos no UniversidadeController.php. O método de `index()` por exemplo é bem simples, pois a idéia é que ele retorne tudo do controller específico. Edite o método `index()` que deve ficar como segue: 

```
public function index()
{
  return response()->json(
    Universidade::all()
  );
}
```

Na sequência, vamos editar o método `store()`. Este, é utilizado para criar um novo registro. Nele devemos validar os dados que foram enviados na requisição POST, realizar o cadastro e por fim retornar o dado cadastrado e o status 201. O código do store deve ficar como mostrado abaixo:

```
public function store(Request $request)
{
  $storeData = Validator::make($request->all(), [
    'nome' => 'required|max:255',
    'descricao' => 'required|max:600',
    'dt_fundacao' => 'required',
    'pais_id' => 'required'
  ]);

  if ($storeData->fails()) {
    return response()->json($storeData->messages(), 422);
  }

  return response()->json(
    Universidade::create($request->all()),
    201
  );
}
```

Vamos prosseguir, com o método `show()`. Ele é utilizado para recuperar um registro específico. O código deste método, para recuperar um único registro deve ficar como mostraro abaixo:
```
public function show($id)
{
  return response()->json(
    Universidade::findOrFail($id)
  );
}
```

Vamos então lidar com o método `update()`, o qual é utilizado para atualizar um registro. Nele são recebidos os dados e o 'id' do registro que deve ser atualizado. Atualize o código como mostrado abaixo:
```
public function update(Request $request, $id)
{
  $updateData = Validator::make($request->all(), [
    'nome' => 'required|max:255',
    'descricao' => 'required|max:600',
    'dt_fundacao' => 'required',
    'pais_id' => 'required'
  ]);

  if ($updateData->fails()) {
    return response()->json($updateData->messages(), 422);
  }

  $universidade = Universidade::whereId($id)->update([
    'nome' => $request->input('nome'),
    'descricao' => $request->input('descricao'),
    'dt_fundacao' => $request->input('dt_fundacao'),
  ]);

  return response()->json($universidade);
}

```

Por fim, chegamos ao método `destroy()`, cujo objetivo é remover um registro específico de acordo com o id informado. Atualize este método de forma que ele fique como mostrado abaixo:
```
public function destroy($id)
{
  $universidade = Universidade::findOrFail($id);
  $universidade->delete();
  return response()->json(null, 204);
}
```

### Routes
As rotas aqui são mapeadas nas funções dos controllers da maneira 1 para 1. Assim, uma rota `/paises` invoca uma função de PaisController, por exemplo. Como estamos trabalhando como um serviço e não diretamente com aplicações `web` integradas, vamos criar as rotas no diretório: `routes/api.php`. Como vamos criar um serviço, devemos usar os métodos do HTTP: `GET`, `POST`, `PUT` e `DELETE` para interagir com a base de dados, obedecendo desta forma as definições de RESTFul. 

Primeiro, acesso o diretório routes a partir do terminal do Laragon como mostra a figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon15.png" alt="Pasta raiz e comando para entrar em routes"/>
</p>

Em seguida, abra o arquivo api.php

```
nano api.php
```

O conteúdo apresentado é o mostrado a seguir:

```
?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

```

Vamos primeramente importar as duas classes: PaisController e UniversidadeController, e em seguida acrescentar as rotas que são primordias para a nossa aplicação. O código ficará como a seguir:

```
<?php

use App\Http\Controllers\PaisController;
use App\Http\Controllers\UniversidadeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/paises', [PaisController::class, 'index']);
Route::get('/paises/{id}', [PaisController::class, 'show']);
Route::post('/paises', [PaisController::class, 'store']);
Route::put('/paises/{id}', [PaisController::class, 'update']);
Route::delete('/paises/{id}', [PaisController::class, 'destroy']);

Route::get('/universidades', [UniversidadeController::class, 'index']);
Route::get('/universidades/{id}', [UniversidadeController::class, 'show']);
Route::post('/universidades', [UniversidadeController::class, 'store']);
Route::put('/universidades/{id}', [UniversidadeController::class, 'update']);
Route::delete('/universidades/{id}', [UniversidadeController::class, 'destroy']);
```

O resultado final é descrito na tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon16.png" alt="Tela final com a descrição de todas as rotas de acesso"/>
</p>

É possível observar o mapeamento de uma classe e uma função no seguinte formato: `[PaisController::class, 'index']`. Isso quer dizer que, quando invocamos por exemplo a rota `/paises`, chamamos a função index do controller PaisController. As outras rotas seguem a mesma lógica.

### Acessar a Aplicação 

Para interagirmos com a aplicação desenvolvida com base no framework Laravel e utilizando o Laragon, precisamos realizar alguns ajustes para ser possível via browser enviar comandos como GET, POST, PUT, DELETE utilizando qualquer aplicação cliente.

#### Ajustes do Virtual Host do Apache

O Laragon vem por padrão com o servidor Apache configurado e operacional. Veja que desenvolvemos toda a aplicação na pasta C:\laragon\www\. O nome da nossa aplicação é laravel-crud-app e fica localizada dentro da pasta www, que é a raiz do servidor Web Apache. Para confirmar, veja a tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon17.png" alt="Tela da raiz do Apache"/>
</p>

Na sequência, temos que procurar pela pasta sites-available, que contém arquivos de configurações de hosts virtuais. Estando no terminal do laragon, execute o comando a seguir:

```
cd etc\apache2\sites-enabled\
```
e terá como resultado o que é mostrado na tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon18.png" alt="Resultado do acesso a pasta sites-available"/>
</p>

A seguir, crie um arquivo chamado: meus-projetos.conf dentro da pasta sites-enabled

```
nano meus-projetos.conf
```

Dentro deste arquivo, inclua o conteúdo abaixo:

```
<VirtualHost _default_:80>
    DocumentRoot "C:/laragon/www/laravel-crud-app/public"
    ServerName laravel-crud-app.test
    ServerAlias *.laravel-crud-app.test
    <Directory "C:/laragon/www/laravel-crud-app/public">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```

O resultado deve se parecer com a tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon19.png" alt="Criacao do vhost para a aplicação"/>
</p>

Salve o arquivo com o comando CRTL+X, digite Y na sequência e tecle ENTER. Pronto!!!

O próximo passo é acessar o painel do Laragon, como mostra a figura abaixo e clicar em **Recarregar**. 

<p align="center">
  <img target="_blank" src="./imagens/laragon20.png" alt="Tela principal do Laragon"/>
</p>

Na sequência, abra o Chrome, Firefox, ou navegador de sua preferência e digite na barra de endereços:

```
http://laravel-crud-app.test
```

E teremos o nosso virtual host funcionado da forma que foi definida, como destaca a tela abaixo.

<p align="center">
  <img target="_blank" src="./imagens/laragon21.png" alt="Finalização da configuração do vhost e teste no browser"/>
</p>

#### Teste da Aplicação Web

Para testarmos a aplicação Web, vamos utilizar como cliente o software [Postman](https://www.postman.com/). Escolhe a versão para Windows, baixe-o e instale em seu computador. Depois de instalado, execute o postman como mostrado na imagem a seguir

<p align="center">
  <img target="_blank" src="./imagens/laragon22.png" alt="Execução do postman"/>
</p>

Ao iniciar o postman, voce terá uma tela inicial parecida como a figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon23.png" alt="Tela inicial do postman"/>
</p>

Para o primeiro teste, considerando a tela anterior, clique no botão **New**, bem no canto superior esquerdo do painel do postman e terá como saída a tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/laragon24.png" alt="Tela de teste HTTP do postman"/>
</p>

Clique na sequência em **HTTP Request** e teremos acesso à tela para enviarmos comandos HTTP para popular ou recuperar dados da aplição no banco de dados do MySQL. O resultado é mostrado abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon25.png" alt="Tela para passarmos comandos para o postman"/>
</p>

O próximo passo é ajustar alguns comandos no postman. 

```
Escolha a opção POST
Digite na barra de endereço ao lado do POST, o seguinte: http://laravel-crud-app.test/api/paises
Setar a opção body
Em seguida escolhar raw 
Depois mude a opção Text para json
```
Depois de ajustada, teremos a tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon26.png" alt="Comando ajustados no postman"/>
</p>

Agora inclua o conteúdo no Body:

```
{"nome": "México" } 
```
E clique no botão SEND. A saída final é dada pela tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/laragon27.png" alt="Saida final com o comando POST"/>
</p>

Veja que já havia um registro inserido na base de dados, e por isso o id do país México é 2. Vamos verifcar como acessar a informação cadastrada por meio do GET, mas no navegador. Digite no navegador o comando:

```
http://laravel-crud-app.test/api/paises
```
E verá que a saída final é a recuperação da base de dados de 2 países cadastrados.

<p align="center">
  <img target="_blank" src="./imagens/laragon28.png" alt="Saida final com o comando GET no Browser"/>
</p>

#### Para Praticar

Considere as outras rotas que codificamos anteriormente como: recuperar um país pelo id, inserir uma universidade, recuperar uma universidade pelo id, apagar universidades, atualizar universidades pelo id, bem como remover os países e também atulizar um país pelo id. Utilize o postman para incluir, apagar e atualizar os dados na aplicação. Fiquem atentos ao conteúdo que deve ser passado no corpo da requisição HTTP. Por exemplo, no caso das universidades, ao criar uma requisição HTTP passando um conteúdo no formato json, você não pode se esquecer que todos os paramêtros precisam ser preenchidos, pois definimos isso como obrigatórios no esquema da tabela Universidades lá no banco de dados. 

É isso aí pessoal. Espero que aproveitem este material e que ele sirva de apoio para você se aprofundar no desenvolvimento de soluções que envolvem o back-end de uma aplicação Web. Até a próxima!!!
